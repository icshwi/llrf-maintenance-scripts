import sys
import shutil

newfile = "tmp.txt"

if (len(sys.argv) != 4):
    print("Usage: udpate_sample_ms.py <path_autosave_file> <freqsamp> <neariqn>")
    print("Example: udpate_sample_ms.py /tmp/settings.sav 117.403 14")
    exit(0)

filename = sys.argv[1]
freqsamp = float(sys.argv[2])
neariqn  = float(sys.argv[3])

conv = neariqn/(freqsamp*1000)

with open(filename) as f:
    with open(newfile, "w") as newf:
        for l in f.readlines():
            newl = l
            if len(l.split("SMonAvgPos")) > 1 or len(l.split("SMonAvgWid")) > 1 or len(l.split("RefLineAvgPos")) > 1 or len(l.split("RefLineAvgWid")) > 1:
                print(l)
                val = int((l.split(" ")[1]).split("\n")[0])
                newval = round(val*conv, 4)
                print(val, newval)
                newl = l.replace(" " + str(val), " " + str(newval))
            newf.write(newl)

shutil.move(newfile, filename)

