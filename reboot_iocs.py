"""Reboot one or many IOCs"""
import epics

filename = "iocs.txt"
with open(filename) as f:
    for l in f.readlines():
        epics.caput(l.split("\n")[0] + ":SysReset", 1)
