# Inputs :
# prefix
# IOC repository
import os
import glob
import pathlib

CALIBVM="ess-facility-info-vm.cslab.esss.lu.se"
PATHCALIBFILES="/var/www/html/ess-viewer/backend/calibTables"

PREFIX="DTL-050_RFS-DIG"
#REPO="https://gitlab.esss.lu.se/ioc/llrf/e3-ioc-llrf-dtl-5.git"
REPO="https://gitlab.esss.lu.se/gabrielfedel/teste_upd_calib"

repo = REPO
prefix = PREFIX

# save current path
path = pathlib.Path(__file__).parent.absolute()

# create folder for files
os.mkdir("/tmp/calib" + prefix)
os.chdir("/tmp/calib" + prefix)

# Download .csv files
os.system("scp %s:%s/%s* ." % (CALIBVM, PATHCALIBFILES, prefix))

# Clean csv files
file_names = glob.glob('*.csv')
for file_name in file_names:
    file_result_name = file_name[:-4] + "_clean.csv"
    # clean the last comment lines
    with open(file_name, "r") as f:
        lines = f.readlines()
        while lines[-1][0] == "#":
            lines = lines[:-1]
        with open(file_result_name, "w") as f_result:
            f_result.writelines(lines)

## rename to the original names
file_names_clean = glob.glob('*_clean.csv')
for file_name in file_names_clean:
    os.rename(file_name, file_name.replace("_clean", ""))

# push csv files to repository
os.chdir("/tmp")
os.system("git clone %s" % (repo))
dir_repo = repo.split("/")[-1].split(".git")[0]
os.chdir("/tmp/%s" % (dir_repo))
##check if calib folder already exist
if os.path.isdir("/tmp/%s/calib" % dir_repo):
    os.system("mv ../%s/*.csv ./calib/" % ("calib" + prefix) )
    os.system("rmdir /tmp/%s" % ("calib" + prefix))
else:
    os.system("mv ../%s ./calib" % ("calib" + prefix) )

##copy script to reload values
os.system("cp %s/load_calibrations.py ./calib" % (path))

os.system("git add .")
os.system("git commit -v -m 'Update calibration files'")
os.system("git push origin master")


# clean tmp files
os.chdir("../")
os.system("rm -rf /tmp/%s" % (dir_repo))

# Message
print("\n\n===== Repository %s updated with new calibration files ====" % repo)
print("To update the calibration on IOC go to the uTCA, on the IOC folder as iocuser and execute:")
print("'git pull origin master'")
print("'cd calib'")
print("'python3 load_calibrations.py'")
