# Scripts for LLRF maintenance

## update_calib

Pull files from calib server and, clean it and push to IOC repository

## load_calibrations

Script to be executed on mtca to update calibration on IOC

## saveOldCalibFiles

Copy csv files from a specific instance renaming to use the PV prefix as the 
file name.
This script should be used to copy the right csv files before the update of 
sis8300llrf to version 5.6 and higher.

## load_calibrations_PV

Script to load all csv files in a folder to the right EGU and Raw PVs
to be used on sis8300llrf 5.6 or higher.
This script might be used with saveOldCalibFiles.
