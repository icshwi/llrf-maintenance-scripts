import os
import json
import argparse
import p4p
import helpers
from p4p.client.thread import Context


class EpicsException(BaseException):
    pass


class ApplicationError(BaseException):
    pass


def get_list_pv(ctx, pv_name):
    content = ctx.get(pv_name, timeout=5)
    pv_list = content.items()[0][1]
    return pv_list


def get_pv_value(ctx, pv):
    pv_read = ctx.get(pv, timeout=5)
    pv_type = type(pv_read).__name__
    raw_value = pv_read.raw.items()[0][1]

    if isinstance(pv_read, p4p.nt.enum.ntenum):
        print(pv)
        value = raw_value.index
        str_value = raw_value.choices[raw_value.index]
    elif isinstance(pv_read, p4p.nt.scalar.ntnumericarray):
        value = raw_value.tolist()
        str_value = str(value)
    else:
        value = raw_value
        str_value = str(value)

    return {'pv': pv, 'type': pv_type, 'value': value, 'str_value': str_value}


def check_pvs(prefix, output_file, save=False, print_json=False):
    save_res_pv_name = helpers.get_save_res_pv(prefix)
    print("Save res pv name: ", save_res_pv_name)
    print("Output file: ", output_file)

    ctx = Context('pva')
    save_res_pv_list = get_list_pv(ctx, save_res_pv_name)

    pv_data = {save_res_pv_name: []}
    save_res_pv_data = []

    for pv in save_res_pv_list:
        save_res_pv_data.append(get_pv_value(ctx, pv))

    pv_data = {save_res_pv_name: save_res_pv_data}

    json_object = json.dumps(pv_data, indent=4)

    if print_json:
        print(json_object)

    if save:
        current_dir = os.getcwd()
        file_path = os.path.join(current_dir, output_file)
        helpers.write_to_file(file_path, json_object)


def main():
    parser = argparse.ArgumentParser(description="Script to read all PV-s marked as save and restore")
    parser.add_argument("prefix", type=str, help="System Prefix")
    parser.add_argument('--output_file', type=str, help='Output file name', default="")
    parser.add_argument('--save', help="Save to file", default=False, action='store_true')
    parser.add_argument('--print', help="Print output json", default=False, action='store_true')
    parser.add_argument('--when', type=str, help='Indicate when pv check is done', default="before", choices=["before", "after"])
    parser.set_defaults(save=True)

    args = parser.parse_args()
    prefix_fixed = args.prefix.replace(':', '_').strip()

    output_file = args.output_file if args.output_file else f"{prefix_fixed}_{args.when}.json"
    check_pvs(args.prefix, output_file, args.save, args.print)


if __name__ == "__main__":
    main()
