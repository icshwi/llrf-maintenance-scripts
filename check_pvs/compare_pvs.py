import os
import json
import argparse
import p4p
import helpers
from p4p.client.thread import Context


def get_pv_data_to_dict(pv_data):
    pv_dict = {}
    for p in pv_data:
        pv_dict[p['pv']] = {'value': p['value'], 'type': p['type']}
    return pv_dict


def get_single_diff_dict(pv_name, pv_dict_before, pv_dict_after):
    value_not_found_data = {'value': '', 'type': ''}
    try:
        pv_data_before = pv_dict_before[pv_name]
    except KeyError:
        pv_data_before = value_not_found_data

    try:
        pv_data_after = pv_dict_after[pv_name]
    except KeyError:
        pv_data_after = value_not_found_data

    return {
               "pv": pv_name,
               "old_value": pv_data_before['value'],
               "new_value": pv_data_after['value'],
               "old_type": pv_data_before['type'],
               "new_type": pv_data_after['type']
           }


def compare_pvs(prefix, file_before_path, file_after_path, output_file,
                save=False, print_json=False):
    with open(file_before_path) as file_before:
        pv_data_before = json.load(file_before)
        pv_data_before_keys = pv_data_before.keys()

    with open(file_after_path) as file_after:
        pv_data_after = json.load(file_after)
        pv_data_after_keys = pv_data_after.keys()

    save_res_pv = helpers.get_save_res_pv(prefix)

    if save_res_pv not in pv_data_before_keys or save_res_pv not in pv_data_after_keys:
        print("Save res PV not found")
        exit(1)

    pv_dict_before = get_pv_data_to_dict(pv_data_before[save_res_pv])
    pv_dict_after = get_pv_data_to_dict(pv_data_after[save_res_pv])

    diff_dict = {}
    diff_dict[save_res_pv] = []

    all_pvs = set(list(pv_dict_before.keys()) + list(pv_dict_after.keys()))
    for pv_name in all_pvs:
        single_diff_dict = get_single_diff_dict(pv_name, pv_dict_before, pv_dict_after)
        if single_diff_dict['old_value'] != single_diff_dict['new_value']:
            diff_dict[save_res_pv].append(single_diff_dict)

    json_object = json.dumps(diff_dict, indent=4)

    if print_json:
        print(json_object)

    if save:
        current_dir = os.getcwd()
        file_path = os.path.join(current_dir, output_file)
        helpers.write_to_file(file_path, json_object)


def main():
    parser = argparse.ArgumentParser(description="Script to read all PV-s marked as save and restore")
    parser.add_argument("prefix", type=str, help="System Prefix")
    parser.add_argument('file_before', type=str, help='Before file name')
    parser.add_argument('file_after', type=str, help='After file name')
    parser.add_argument('--output_file', type=str, help='Output file name', default="")
    parser.add_argument('--save', help="Save to file", default=False, action='store_true')
    parser.add_argument('--print', help="Print output json", default=False, action='store_true')

    args = parser.parse_args()

    prefix_fixed = args.prefix.replace(':', '_').strip()
    output_file = args.output_file if args.output_file else f"{prefix_fixed}_diff.json"

    compare_pvs(args.prefix, args.file_before, args.file_after, output_file, args.save, args.print)


if __name__ == "__main__":
    main()
