def write_to_file(file_name, data):
    try:
        with open(file_name, 'w+') as file:
            file.write(data)
    except Exception as e:
        print(f"Problem with writing to file {file_name}: {e}")
        exit(1)


def get_save_res_pv(prefix):
    return f"{prefix}:SavResList"


def get_archiver_pv(prefix):
    return f"{prefix}:ArchiverList"

