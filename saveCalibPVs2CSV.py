import csv
from epics import caget

file_name = "tiger-ff.csv"
prefix = "TIGER-010:RFS-DIG-101:RFCtrlFF"

col1 = caget(prefix + "-CalEGU")
col2 = caget(prefix + "-CalRaw")
with open(file_name, "w") as csvfile:
    writer = csv.writer(csvfile)
    for i in range(len(col1)):
        writer.writerow([col1[i], col2[i]])
