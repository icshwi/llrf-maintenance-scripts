from epics import caget
import sys
import os
import shutil
import csv

if (len(sys.argv) < 2):
    print("You must inform the prefix in the format 'TS2-010RFC:RFS-DIG-201'")
    exit(0)   

prefix = sys.argv[1]
prefix_clean = prefix.replace(":","_")
# Create folder for save the files
if not os.path.exists(prefix_clean):
    os.makedirs(prefix_clean)

# AI Channels
for i in range(10):
    filename = caget(prefix + ":AI%d-CalCSV" % i, as_string=True)
    if filename != "":
        print(filename)
        shutil.copy(filename, prefix_clean + "/" + prefix_clean + "_AI%d.csv" % i)

# FF Calib
file_name = prefix_clean+"_RFCtrlFF.csv"

col1 = caget(prefix + "RFCtrlFFCalEGU")
col2 = caget(prefix + "RFCtrlFFCalRaw")
with open(file_name, "w") as csvfile:
    writer = csv.writer(csvfile)
    for i in range(len(col1)):
        writer.writerow([col1[i], col2[i]])
